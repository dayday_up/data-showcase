export const chartCommonConfig = {
    color: [
        "#1de9b6",
        "#ff3d00",
        "#ff9100",
        "#ff1744",
        "#d500f9",
        "#651fff",
        "#2979ff",
        "#00b0ff",
        "#00e676",
        "#c6ff00",
        "#ffea00"
    ],
    grid: {
        left: 100,
        right: 0,
        top: 80,
        show: true,
        borderColor: "#0a3c51"
    },
    title: {
        textStyle: {
            color: "#8C9BA4",
            fontSize: 22
        }
    },
    axisLine: {
        lineStyle: {
            color: "#8C9BA4"
        }
    },
    tooltip: {
        trigger: 'axis'
    }
}