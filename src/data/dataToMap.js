import { workshopsData, maintenanceData, equipmentsData } from './data';
const tempPointsMap = new Map();
equipmentsData.forEach(equipment => {
    const points = equipment.points;
    points.forEach(point => {
        tempPointsMap.set(point.id, point);
    }) 
})

export const dataToMap = function (data) {
    const map = new Map();
    data.forEach(element => {
        map.set(element.id, element)
    });
    return map;
}

export const workshopsMap = dataToMap(workshopsData);
export const maintenanceMap = dataToMap(maintenanceData);
export const equipmentsMap = dataToMap(equipmentsData);
export  const pointsMap = tempPointsMap;