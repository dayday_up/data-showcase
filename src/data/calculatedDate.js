import { workshopsMap, maintenanceMap, equipmentsMap, pointsMap } from "./dataToMap";

export const getTotalOilOfEachWorkShop = function () {
    const map = new Map();
    maintenanceMap.forEach(element => {
        if (element.type === "停机") {
            return false;
        }
        const equipmentId = element.equipmentId;
        const equipment = equipmentsMap.get(equipmentId);
        const pointId = element.pointId;
        const point = pointsMap.get(pointId);
        const workshopId = equipment.workshopId;
        const workshop = workshopsMap.get(workshopId);
        const value = map.get(workshop.name);
        if (!value) {
            const value = {
                type: {},
                brand: {}
            };
            const volume = element.volume;
            value["总用油量"] = volume;
            value.type[equipment.type] = volume;
            value.brand[point.oilBrand] = volume;
            map.set(workshop.name, value);
            return false;
        }
        value["总用油量"] = value["总用油量"] + element.volume;
        value.brand[point.oilBrand] = value.brand[point.oilBrand] + element.volume || element.volume;
        value.type[equipment.type] = value.type[equipment.type] + element.volume || element.volume;
    });
    const resultArray = Array.from(map);
    return resultArray.sort((prev, next) => {
        return prev[0].slice(2) - next[0].slice(2);
    });
}

export const getTotalOilOfEachEquipment = function () {
    const map = new Map();
    maintenanceMap.forEach(element => {
        if (element.type === "停机") {
            return false;
        }
        const equipmentId = element.equipmentId;
        const equipment = equipmentsMap.get(equipmentId);
        const value = map.get(equipment.type);
        map.set(equipment.type, value + element.volume || element.volume)
    });
    return Array.from(map);
}

export const getTotalOilOfEachBrand = function () {
    const map = new Map();
    maintenanceMap.forEach(element => {
        if (element.type === "停机") {
            return false;
        }
        const pointId = element.pointId;
        const point = pointsMap.get(pointId);
        const value = map.get(point.oilBrand);
        map.set(point.oilBrand, value + element.volume || element.volume)
    });
    return Array.from(map);
}